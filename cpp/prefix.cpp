#include <string>
#include <vector>
#include <iostream>

std::string longestPrefix(const std::vector<std::string> & input) {
  if (input.size() == 0) {
    return "";
  }

  const std::string & prefix = input[0];
  for ( size_t j = 0; j < prefix.size(); ++j ) {
    for( size_t i = 1; i < input.size(); ++i ) {
      if(input[i].size() < j || input[i][j] != prefix[j]) {
        return prefix.substr(0, j);
      }
    }
  }
  return prefix;
}

int main(int argc, char* argv[]) {
  std::vector<std::string> args;
  for (size_t i = 1; i < argc; ++i) {
    args.push_back(std::string(argv[i]));
  }
  std::cout << longestPrefix(args) << std::endl;
}