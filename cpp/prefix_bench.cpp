#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <benchmark/benchmark.h>

std::string longestPrefix(const std::vector<std::string> & input) {
  if (input.size() == 0) {
    return "";
  }

  const std::string & prefix = input[0];
  for ( size_t j = 0; j < prefix.size(); ++j ) {
    for( size_t i = 1; i < input.size(); ++i ) {
      if(input[i].size() < j || input[i][j] != prefix[j]) {
        return prefix.substr(0, j);
      }
    }
  }
  return prefix;
}

static  std::vector<std::string> args;

static void DoSetup(const benchmark::State& state) {
	std::ifstream infile("../testdata/dev-tree");
	std::string line;
	while (std::getline(infile, line)) {
    		std::istringstream iss(line);
  		args.push_back(line);
	}
}
static void BM_LongestPrefix(benchmark::State& state) {
  for (auto _ : state)
    longestPrefix(args);
}

BENCHMARK(BM_LongestPrefix)->Setup(DoSetup);

BENCHMARK_MAIN();

