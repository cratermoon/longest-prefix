package prefix

import (
	"bufio"
	"os"
	"testing"
)

var randomInput = []string{
	"bIfkYN0bYY",
	"OhkHQUDIwA",
	"AKhMTABLN9",
	"KueF5rg4jM",
	"JKXpw6HUZV",
	"if9r6bxeCx",
	"SyWft1374O",
	"cmoDm38t34",
	"H3Nwp78xSL",
	"UXgXxzBHIN",
	"lHx6kSeFJE",
	"7wOUyolvE0",
	"c2vrm1cEC6",
	"ADfMsxFw64",
	"FbkAVuyoz5",
	"gN2VCYq6Oh",
	"qq5WFpOAK2",
	"w7C34mhFeK",
	"hpMA2t59nQ",
	"AZ0U2Z2ibf",
}

var presortedInput = []string{
	"7wOUyolvE0",
	"ADfMsxFw64",
	"AKhMTABLN9",
	"AZ0U2Z2ibf",
	"FbkAVuyoz5",
	"H3Nwp78xSL",
	"JKXpw6HUZV",
	"KueF5rg4jM",
	"OhkHQUDIwA",
	"SyWft1374O",
	"UXgXxzBHIN",
	"c2vrm1cEC6",
	"cmoDm38t34",
	"gN2VCYq6Oh",
	"hpMA2t59nQ",
	"if9r6bxeCx",
	"lHx6kSeFJE",
	"qq5WFpOAK2",
	"w7C34mhFeK",
	"bIfkYN0bYY",
}

var allTheSameWord = []string{
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
	"ADfMsxFw64",
}
var result string

func longestSort(input []string, b *testing.B) {
	var r string
	for n := 0; n < b.N; n++ {
		r = LongestSort(input)
	}
	result = r
}

func longest(input []string, b *testing.B) {
	var r string
	for n := 0; n < b.N; n++ {
		r = Longest(input)
	}
	result = r
}

// strings in random order
func BenchmarkRandom_Presort(b *testing.B) {
	longestSort(randomInput, b)
}

func BenchmarkRandom_Nosort(b *testing.B) {
	longest(randomInput, b)
}

// pre-sorted strings
func BenchmarkSorted_Presort(b *testing.B) {
	longestSort(presortedInput, b)
}

func BenchmarkSorted_Nosort(b *testing.B) {
	longest(presortedInput, b)
}

func BenchmarkAllSame_Presort(b *testing.B) {
	longestSort(allTheSameWord, b)
}

func BenchmarkAllSame_Nosort(b *testing.B) {
	longest(allTheSameWord, b)
}

var inputTree []string

func Tree() []string {
	return inputTree
}
func BenchmarkTree_Nosort(b *testing.B) {
	input := Tree()
	for n := 0; n < b.N; n++ {
		Longest(input)
	}
}
func BenchmarkTree_Presort(b *testing.B) {
	input := Tree()
	for n := 0; n < b.N; n++ {
		LongestSort(input)
	}
}

func TestMain(m *testing.M) {
	f, err := os.Open("../../testdata/dev-tree")
	if err != nil {
		os.Exit(1)
	}
	defer f.Close()
	// we know the data is about 400 lines long)
	inputTree = make([]string, 0, 400)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		inputTree = append(inputTree, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		os.Exit(1)
	}
	// makes the dataset twice as long
	// gives sort something to do
	inputTree = append(inputTree, inputTree...)

	os.Exit(m.Run())
}
