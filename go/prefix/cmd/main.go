package main

import (
	"flag"
	"fmt"
	"time"

	"prefix"
)

func bench(args []string, name string, f func([]string) string) {
	t := time.Now()
	for i := 0; i < 999999; i++ {
		f(args)
	}
	fmt.Printf("%7s: %v\n", name, time.Since(t))

}
func main() {
	flag.Parse()
	args := flag.Args()
	bench(args, "sort", prefix.LongestSort)
	bench(args, "no sort", prefix.Longest)
}
