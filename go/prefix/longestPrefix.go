package prefix

import (
	"sort"
)

func Longest(input []string) string {
	if len(input) == 0 {
		return ""
	}

	prefix := input[0]
	length := len(prefix)
	for i, s := range input {
		if len(s) < length {
			length = len(s)
		}

		for j := 0; j < length; j++ {
			if input[i][j] != prefix[j] {
				length = j
				break
			}

			if length == 0 {
				break
			}
		}

	}
	return prefix[0:length]
}

func LongestSort(input []string) string {
	if len(input) == 0 {
		return ""
	}
	sort.Strings(input)
	first := input[0]
	last := input[len(input)-1]
	length := len(first)
	for i := 0; i < length; i++ {
		if first[i] != last[i] {
			length = i
			break
		}
	}
	return first[0:length]
}
