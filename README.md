# Longest Common Prefix

Which is faster:

- sort the list and compare the first and last,
- check every item to the length of the current longest common prefix

## Techniques

### Horizontal Scan

$W$ is the number of strings, $L$ is the length of the longest string

$O(W*L)$

### Vertical Scan

$O(W*L)$

### Divide and Conquer

$O(W*L)$

### Binary Search

### Sort

$O(W*L log L)$

Sorting takes $O(N log N)$ time with standard library algorithms, and comparing the first and last strings takes $O(L)$ time.

## Go

[How to write benchmarks in Go](https://dave.cheney.net/2013/06/30/how-to-write-benchmarks-in-go)

To run the Go benchmarks

`go test -bench=.`

## References

[Longest Common Prefix at leetcode](https://leetcode.com/problems/longest-common-prefix/description/)
